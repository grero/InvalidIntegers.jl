module InvalidIntegers
import Base:+,-,*,/

struct InvalidInteger{T<:Integer} <: Integer
    val::T
end

(+)(x::T, y::InvalidInteger{T}) where T <: Integer = InvalidInteger(x+y.val)
(+)(x::InvalidInteger{T}, y::T) where T <: Integer = InvalidInteger(x.val+y)

(-)(x::T, y::InvalidInteger{T}) where T <: Integer = InvalidInteger(x-y.val)
(-)(x::InvalidInteger{T}, y::T) where T <: Integer = InvalidInteger(x.val-y)

(*)(x::T, y::InvalidInteger{T}) where T <: Integer = InvalidInteger(x*y.val)
(*)(x::InvalidInteger{T}, y::T) where T <: Integer = InvalidInteger(x.val*y)

# lazy way of doing it, since normal division with integer is cast as a float, just return NaN
for (T,N) in zip([Int16, Int32, Int64], [NaN16, NaN32, NaN64])
    eval(
    quote
        (/)(x::$T, y::InvalidInteger{$T}) = $N
        (/)(x::InvalidInteger{$T}, y::$T) = $N
    end
   )

end

end # module
