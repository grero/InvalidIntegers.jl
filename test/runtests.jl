using InvalidIntegers
using Test

@testset "Basic" begin
    a = 2
    b = InvalidIntegers.InvalidInteger(3)
    c = a+b
    @test c == InvalidIntegers.InvalidInteger(5)
    c = a*b
    @test c == InvalidIntegers.InvalidInteger(6)
    c = a-b
    @test c == InvalidIntegers.InvalidInteger(-1)
    c = a/b
    @test isnan(c)
end
